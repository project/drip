<?php

/**
 * @file
 * Administrative page callbacks for the Drip module.
 */

/**
 * Implements hook_admin_settings() for module settings configuration.
 */
function drip_admin_settings_form($form, &$form_state) {

  $form['drip_account'] = array(
    '#title' => t('Drip ID'),
    '#type' => 'textfield',
    '#default_value' => variable_get('drip_account', ''),
    '#size' => 15,
    '#maxlength' => 20,
    '#required' => TRUE,
    '#description' => t('This ID is unique for your Drip account'),
  );

  return system_settings_form($form);
}
