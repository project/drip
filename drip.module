<?php
/**
 * @file
 * Drupal Module: Drip
 *
 * Adds the required Javascript to all your Drupal pages to allow tracking by
 * the Drip for Marketing Automation
 *
 * @author: Blair Wadman <https://www.drupal.org/u/blairski>
 */

/**
 * Implements hook_menu().
 */
function drip_menu() {
  $items['admin/config/system/drip'] = array(
    'title' => 'Drip',
    'description' => 'Configure settings for Drip',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('drip_admin_settings_form'),
    'access arguments' => array('administer users'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'drip.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_page_alter() to insert JavaScript to the appropriate scope/region of the page.
 */
function drip_page_alter(&$page) {

  $id = variable_get('drip_account');
  $drip = '<!-- Drip -->
           var _dcq = _dcq || [];
          var _dcs = _dcs || {};
          _dcs.account = \''. $id .'\';

          (function() {
            var dc = document.createElement(\'script\');
            dc.type = \'text/javascript\'; dc.async = true;
            dc.src = \'//tag.getdrip.com/'. $id .'.js\';
            var s = document.getElementsByTagName(\'script\')[0];
            s.parentNode.insertBefore(dc, s);
          })();';

  drupal_add_js($drip, array(
      'type' => 'inline',
      'scope' => 'footer',
      'requires_jquery' => FALSE,
      'weight' => 10
    )
  );
}
